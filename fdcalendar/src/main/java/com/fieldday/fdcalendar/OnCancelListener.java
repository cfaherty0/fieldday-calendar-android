package com.fieldday.fdcalendar;

public interface OnCancelListener {
	abstract void onCancel(FDMonthView view);
}

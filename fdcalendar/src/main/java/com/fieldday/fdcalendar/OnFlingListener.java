package com.fieldday.fdcalendar;

import android.view.MotionEvent;

public interface OnFlingListener {
	abstract void onFling(FDMonthGridView view, MotionEvent event1, MotionEvent event2, float velocityX, float velocityY);
}

package com.fieldday.fdcalendar;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class FDMonthView extends LinearLayout implements OnFlingListener {
	Context context;
	int month;
	int year;
	OnCancelListener onCancelListener = null;
	OnSaveListener onSaveListener = null;
	OnSeeActivityTimesListener onSeeActivityTimesListener = null;

	public FDMonthView(Context context, int month, int year, FDApptData[] notAvailable, FDApptData[] selected) {
		super(context);
		this.context = context;
		this.month = month;
		this.year = year;
		this.init();
	}

	public FDMonthView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		this.month = 1;
		this.year = 2021;
		this.init();
	}

	void init() {
		this.setOrientation(LinearLayout.VERTICAL);
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		this.setLayoutParams(params);
		LayoutInflater inflater = (LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.fdcalendar, this);
		this.updateDate(this.month, this.year);
		View buttonPrev = this.findViewById(R.id.buttonPrev);
		buttonPrev.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				FDMonthView parent = FDMonthView.this;
				FDDate p_m = FDMonthGridView.prevMonth(parent.month, parent.year);
				parent.updateDate(p_m.month, p_m.year);
			}
		});
		View buttonNext = this.findViewById(R.id.buttonNext);
		buttonNext.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				FDMonthView parent = FDMonthView.this;
				FDDate p_m = FDMonthGridView.nextMonth(parent.month, parent.year);
				parent.updateDate(p_m.month, p_m.year);
			}
		});
		View buttonCancel = this.findViewById(R.id.buttonCancel);
		buttonCancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (FDMonthView.this.onCancelListener != null) {
					FDMonthView.this.onCancelListener.onCancel(FDMonthView.this);
				}
			}
		});
		View buttonSave = this.findViewById(R.id.buttonSave);
		buttonSave.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (FDMonthView.this.onSaveListener != null) {
					FDMonthView.this.onSaveListener.onSave(FDMonthView.this);
				}
			}
		});
		View buttonSeeActivityTimes = this.findViewById(R.id.buttonSeeActivityTimes);
		buttonSeeActivityTimes.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (FDMonthView.this.onSeeActivityTimesListener != null) {
					FDMonthView.this.onSeeActivityTimesListener.onSeeActivityTimes(FDMonthView.this);
				}
			}
		});
		FDMonthGridView grid = this.findViewById(R.id.FDMonthGridView);
		grid.setOnFlingListener(this);
	}

	public void setMultiSelection(Boolean multiSelection) {
		FDMonthGridView grid = this.findViewById(R.id.FDMonthGridView);
		grid.setMultiSelection(multiSelection);
	}

	public void setCancelHandler(OnCancelListener listener)	{
		this.onCancelListener = listener;
	}

	public void setSaveHandler(OnSaveListener listener) {
		this.onSaveListener = listener;
	}

	public void setSeeActivityTimesHandler(OnSeeActivityTimesListener listener) {
		this.onSeeActivityTimesListener = listener;
	}

	public FDApptData[] getSelected() {
		FDMonthGridView grid = this.findViewById(R.id.FDMonthGridView);
		return grid.getSelected();
	}

	void updateDate(int month, int year) {
		this.month = month;
		this.year = year;
		TextView textDate = this.findViewById(R.id.textDate);
		textDate.setText(FDMonthGridView.monthLabel(this.month) + " " + this.year);
		FDMonthGridView grid = this.findViewById(R.id.FDMonthGridView);
		grid.updateDate(this.month, this.year);
	}

	@Override
	public void onFling(FDMonthGridView view, MotionEvent event1, MotionEvent event2, float velocityX, float velocityY) {
		if (velocityX >= 200) {
			FDDate p_m = FDMonthGridView.prevMonth(this.month, this.year);
			this.updateDate(p_m.month, p_m.year);
		} else if (velocityX <= -200) {
			FDDate p_m = FDMonthGridView.nextMonth(this.month, this.year);
			this.updateDate(p_m.month, p_m.year);
		}
	}
}

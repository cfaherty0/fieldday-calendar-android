package com.fieldday.fdcalendar;

public interface OnSaveListener {
	abstract void onSave(FDMonthView view);
}

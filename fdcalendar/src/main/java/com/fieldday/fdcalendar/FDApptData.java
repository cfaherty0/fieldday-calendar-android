package com.fieldday.fdcalendar;

public class FDApptData {
    public FDApptDataType type;
    public int day;
    public int month;
    public int year;

    public FDApptData(FDApptDataType type, int day, int month, int year) {
        this.type = type;
        this.day = day;
        this.month = month;
        this.year = year;
    }
}

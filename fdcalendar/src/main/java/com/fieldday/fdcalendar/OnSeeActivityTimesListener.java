package com.fieldday.fdcalendar;

public interface OnSeeActivityTimesListener {
	abstract void onSeeActivityTimes(FDMonthView view);
}

package com.fieldday.fdcalendar;

public enum FDApptDataType {
	DAY,
	BEFORE,
	AFTER
}

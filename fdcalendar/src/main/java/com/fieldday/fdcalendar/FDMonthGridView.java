package com.fieldday.fdcalendar;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.support.v4.view.GestureDetectorCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.Arrays;

public class FDMonthGridView extends View implements GestureDetector.OnGestureListener {
	private final Context context;
	int month;
	int year;
	FDApptData[] notAvailable;
	FDApptData[] selected;
	FDApptData[] newSelected;
	View child;
	int fontSizeMedium;
	int headerHeight;
	FDDayData[] dayData;
	boolean multiSelection = true;
	private GestureDetectorCompat mDetector;
	OnFlingListener onFlingListener = null;

	public FDMonthGridView(Context context, int month, int year, FDApptData[] notAvailable, FDApptData[] selected) {
		super(context);
		this.context = context;
		this.month = month;
		this.year = year;
		this.notAvailable = notAvailable;
		this.selected = selected;
		this.newSelected = selected;
		this.init();
	}

	public FDMonthGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		this.month = 1;
		this.year = 2021;
		FDApptData[] demoNotAvailable = {
				new FDApptData(FDApptDataType.DAY, 1, 1, 2021),
				new FDApptData(FDApptDataType.BEFORE, 1, 11, 2020)
		};
		FDApptData[] demoSelected = {
				new FDApptData(FDApptDataType.DAY, 2, 1, 2021),
				new FDApptData(FDApptDataType.DAY, 3, 1, 2021),
				new FDApptData(FDApptDataType.DAY, 4, 1, 2021)
		};
		this.notAvailable = demoNotAvailable;
		this.selected = demoSelected;
		this.newSelected = demoSelected;
		this.init();
	}

	@SuppressLint("ClickableViewAccessibility")
	void init() {
		this.fontSizeMedium = getResources().getDimensionPixelSize(R.dimen.FDCalendarFontSizeMedium);
		this.headerHeight = getResources().getDimensionPixelSize(R.dimen.FDCalendarHeaderHeight);
		this.dayData = new FDDayData[0];
		final int[] initPos = {0, 0};
		this.mDetector = new GestureDetectorCompat(this.context, this);
		this.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (FDMonthGridView.this.mDetector.onTouchEvent(event)) {
					return true;
				}
				FDDayData[] dayData = FDMonthGridView.this.dayData;
				int x = (int)event.getX();
				int y = (int)event.getY() - FDMonthGridView.this.headerHeight;
				switch (event.getAction()) {
					case MotionEvent.ACTION_DOWN:
						initPos[0] = x;
						initPos[1] = y;
						break;
					case MotionEvent.ACTION_UP:
						// Which date?
						for (int i = 0; i < dayData.length; i++) {
							if (initPos[0] >= dayData[i].x && initPos[1] >= dayData[i].y && initPos[0] < dayData[i].x + dayData[i].w && initPos[1] < dayData[i].y + dayData[i].h &&
								x >= dayData[i].x && y >= dayData[i].y && x < dayData[i].x + dayData[i].w && y < dayData[i].y + dayData[i].h) {
								FDMonthGridView.this.toggleDayData(dayData[i].day, dayData[i].month, dayData[i].year);
								FDMonthGridView.this.invalidate();
								break;
							}
						}
						break;
				}
				return true;
			}
		});
	}

	@Override
	public boolean onDown(MotionEvent e) {
		return false;
	}

	@Override
	public void onShowPress(MotionEvent e) {
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		return false;
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {
	}

	@Override
	public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY) {
		if (this.onFlingListener != null) {
			this.onFlingListener.onFling(this, event1, event2, velocityX, velocityY);
		}
		return false;
	}

	public FDApptData[] getSelected() {
		return this.newSelected;
	}

	public void updateDate(int month, int year) {
		this.month = month;
		this.year = year;
		this.invalidate();
	}

	public void setMultiSelection(boolean multiSelection) {
		this.multiSelection = multiSelection;
	}

	public void setOnFlingListener(OnFlingListener onFlingListener) {
		this.onFlingListener = onFlingListener;
	}

	void createHeader(Canvas canvas, int x, int y, int w) {
		int xi = w / 7;
		int x2 = (w - xi * 7) / 2;
		Paint mPaintText = new Paint();
		mPaintText.setTypeface(Typeface.DEFAULT);
		mPaintText.setTextSize(this.fontSizeMedium);
		mPaintText.setTextAlign(Paint.Align.CENTER);
		mPaintText.setColor(Color.rgb(0xF6, 0xF4, 0xF1));
		for (int i = 0; i < 7; i++) {
			canvas.drawText(dowLabel(i), x + x2 + xi / 2, y, mPaintText);
			x2 += xi;
		}
	}

	void createDay(Canvas canvas, int x, int y, int w, int h, int day, int month, int year, int textColor, int backgroundColor) {
		Paint mPaintText = new Paint();
		mPaintText.setTypeface(Typeface.DEFAULT_BOLD);
		mPaintText.setTextSize(this.fontSizeMedium);
		mPaintText.setTextAlign(Paint.Align.CENTER);
		mPaintText.setColor(backgroundColor);
		canvas.drawCircle(x + w / 2, y + h / 2, (w - 12) / 2, mPaintText);
		mPaintText.setColor(textColor);
		String text = Integer.toString(day);
		Rect rect = new Rect();
		mPaintText.getTextBounds(text, 0, text.length(), rect);
		canvas.drawText(text, x + w / 2, y + h / 2 - rect.exactCenterY(), mPaintText);
	}

	void createMonth(Canvas canvas, int x, int y, int w, int h) {
		this.dayData = drawMonth(this.month, this.year, w, h);
		for (int i = 0; i < this.dayData.length; i++) {
			FDDayData dd = this.dayData[i];
			createDay(canvas, x + dd.x, y + dd.y, dd.w, dd.h, dd.day, dd.month, dd.year, dd.textColor, dd.backgroundColor);
		}
	}

	void createLegend(Canvas canvas, int x, int y, int w) {
		String[] text = {"Available", "Not Available", "Selected"};
		int[] colors = {
				Color.rgb(0xF6, 0xF4, 0xF1),
				Color.rgb(0x5A, 0x77, 0x84),
				Color.rgb(0xC3, 0xB4, 0xE2)
		};
		int xi = w / 3;
		int x2 = (w - xi * 3) / 2;
		Paint mPaint = new Paint();
		mPaint.setStrokeWidth(1);
		mPaint.setTypeface(Typeface.DEFAULT);
		mPaint.setTextSize(this.fontSizeMedium);
		mPaint.setTextAlign(Paint.Align.LEFT);
		Rect rect = new Rect();
		mPaint.getTextBounds(text[0], 0, text[0].length(), rect);
		for (int i = 0; i < text.length; i++) {
			int width = (int) mPaint.measureText(text[i]);
			int x3 = x2 + (xi / 2 - width / 2);
			mPaint.setColor(colors[i]);
			canvas.drawCircle(x3, y, this.fontSizeMedium / 2, mPaint);
			mPaint.setColor(Color.rgb(0xF6, 0xF4, 0xF1));
			canvas.drawText(text[i], x3 + this.fontSizeMedium / 2 + 14, y - rect.exactCenterY(), mPaint);
			x2 += xi;
		}
	}

	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int desiredWidth = MeasureSpec.getSize(widthMeasureSpec);
		int desiredHeight = desiredWidth + 2 * this.headerHeight;

		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int widthSize = MeasureSpec.getSize(widthMeasureSpec);
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);
		int heightSize = MeasureSpec.getSize(heightMeasureSpec);

		int width;
		int height;

		//Measure Width
		if (widthMode == MeasureSpec.EXACTLY) {
			//Must be this size
			width = widthSize;
		} else if (widthMode == MeasureSpec.AT_MOST) {
			//Can't be bigger than...
			width = Math.min(desiredWidth, widthSize);
		} else {
			//Be whatever you want
			width = desiredWidth;
		}

		//Measure Height
		if (heightMode == MeasureSpec.EXACTLY) {
			//Must be this size
			height = heightSize;
		} else if (heightMode == MeasureSpec.AT_MOST) {
			//Can't be bigger than...
			height = Math.min(desiredHeight, heightSize);
		} else {
			//Be whatever you want
			height = desiredHeight;
		}

		//MUST CALL THIS
		setMeasuredDimension(width, height);
	}

	protected void onDraw(Canvas canvas) {
		int width = ((View) this.getParent()).getWidth();
		int height = ((View) this.getParent()).getHeight();
		int w = width;
		int h = width;
		createHeader(canvas, 0, this.headerHeight / 2, w);
		createMonth(canvas, 0, this.headerHeight, w, h);
		createLegend(canvas, 0, this.headerHeight + h + this.headerHeight / 2, w);
	}

	FDDayData[] drawMonth(int month, int year, int w, int h) {
		ArrayList<FDDayData> day_data = new ArrayList<FDDayData>();
		int i = 0;
		int day = 1;
		int day_count = daysInMonth(month, year);
		int day_start = dow(1, month, year);
		FDDate p_m = prevMonth(month, year);
		int prev_day_count = daysInMonth(p_m.month, p_m.year);
		int prev_day_start = prev_day_count - day_start;
		int next_day_start = day_count;
		FDDate n_m = nextMonth(month, year);
		int xi = w / 7;
		int yi = h / 6;
		int xs = (w - xi * 7) / 2;
		int ys = (h - yi * 6) / 2;
		int x = xs;
		int y = ys;
		for (int week = 0; week < 6; week++) {
			x = xs;
			xi = w / 7;
			for (int dow = 0; dow < 7; dow++) {
				i++;
				int d_day = 0;
				int d_month = 0;
				int d_year = 0;
				if (i > day_start && day <= day_count) {
					d_day = day;
					d_month = month;
					d_year = year;
					day++;
				} else if (prev_day_start + i <= prev_day_count) {
					d_day = prev_day_start + i;
					d_month = p_m.month;
					d_year = p_m.year;
				} else if (day > next_day_start) {
					d_day = day - next_day_start;
					d_month = n_m.month;
					d_year = n_m.year;
					day++;
				}
				FDColors colors = getColors(d_day, d_month, d_year);
				FDDayData d = new FDDayData(x, y, xi, yi, d_day, d_month, d_year, colors.textColor, colors.backgroundColor);
				day_data.add(d);
				x += xi;
			}
			if (week == 5) {
				yi = h - y;
			}
			y += yi;
		}
		return day_data.toArray(new FDDayData[day_data.size()]);
	}

	FDColors getColors(int day, int month, int year) {
		if (this.checkDayData(this.notAvailable, day, month, year) != -1) {
			// not available
			return new FDColors(Color.rgb(0xF6, 0xF4, 0xF1), Color.rgb(0x5A, 0x77, 0x84));
		}
		if (this.checkDayData(this.newSelected, day, month, year) != -1) {
			// selected
			return new FDColors(Color.rgb(0x27, 0x28, 0x28), Color.rgb(0xC3, 0xB4, 0xE2));
		}
		// available
		return new FDColors(Color.rgb(0x27, 0x28, 0x28), Color.rgb(0xF6, 0xF4, 0xF1));
	}

	int checkDayData(FDApptData[] dayData, int day, int month, int year) {
		int idx = -1;
		for (int i = 0; i < dayData.length && idx == -1; i++) {
			FDApptData e = dayData[i];
			if (e.type == FDApptDataType.DAY) {
				if (e.year == year && e.month == month && e.day == day) {
					idx = i;
				}
			} else if (e.type == FDApptDataType.BEFORE) {
				if (year < e.year ||
						(year == e.year && month < e.month) ||
						(year == e.year && month == e.month && day < e.day) ||
						(year == e.year && month == e.month && day == e.day)) {
					idx = i;
				}
			} else if (e.type == FDApptDataType.AFTER) {
				if (year > e.year ||
						(year == e.year && month > e.month) ||
						(year == e.year && month == e.month && day > e.day) ||
						(year == e.year && month == e.month && day == e.day)) {
					idx = i;
				}
			}
		}
		return idx;
	}

	void toggleDayData(int day, int month, int year) {
		int i = this.checkDayData(this.newSelected, day, month, year);
		if (i != -1) {
			// Already selected, unselect
			ArrayList<FDApptData> a = new ArrayList<>(Arrays.asList(this.newSelected));
			a.remove(i);
			this.newSelected = (FDApptData[])a.toArray(new FDApptData[a.size()]);
			return;
		}
		if (this.checkDayData(this.notAvailable, day, month, year) != -1) {
			// Not available, do nothing
			return;
		}
		if (!this.multiSelection) {
			this.newSelected = new FDApptData[0];
		}
		// Set as selected
		this.newSelected = Arrays.copyOf(this.newSelected, this.newSelected.length + 1);
		this.newSelected[this.newSelected.length - 1] = new FDApptData(FDApptDataType.DAY, day, month, year);
	}

	static int daysInMonth(int month, int year) {
		int[] d = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		int n = d[month - 1];
		if (month == 2 && (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0))) {
			n += 1;
		}
		return n;
	}

	static int dow(int day, int month, int year) {
		int t[] = {0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};
		int y = month < 3 ? year - 1 : year;
		return ((y + (y / 4) - (y / 100) + (y / 400) + t[month - 1] + day) % 7);
	}

	static String dowLabel(int day) {
		String[] labels = {"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"};
		return labels[day];
	}

	static public String monthLabel(int month) {
		String[] labels = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
		return labels[month - 1];
	}

	static public FDDate prevMonth(int month, int year) {
		return month > 1 ? new FDDate(month - 1, year) : new FDDate(12, year - 1);
	}

	static public FDDate nextMonth(int month, int year) {
		return month < 12 ? new FDDate(month + 1, year) : new FDDate(1, year + 1);
	}

}

class FDDayData {
	int x;
	int y;
	int w;
	int h;
	int day;
	int month;
	int year;
	int textColor;
	int backgroundColor;

	FDDayData(int x, int y, int w, int h, int day, int month, int year, int textColor, int backgroundColor) {
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.day = day;
		this.month = month;
		this.year = year;
		this.textColor = textColor;
		this.backgroundColor = backgroundColor;
	}
}

class FDDate {
	int month;
	int year;

	FDDate(int month, int year) {
		this.month = month;
		this.year = year;
	}
}

class FDColors {
	int textColor;
	int backgroundColor;

	FDColors(int textColor, int backgroundColor) {
		this.textColor = textColor;
		this.backgroundColor = backgroundColor;
	}
}

package com.example.myfirstapp;

import android.app.Activity;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.CancellationSignal;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.fieldday.fdcalendar.FDApptData;
import com.fieldday.fdcalendar.FDMonthView;
import com.fieldday.fdcalendar.OnCancelListener;
import com.fieldday.fdcalendar.OnSaveListener;
import com.fieldday.fdcalendar.OnSeeActivityTimesListener;

public class MainActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Button buttonShowCalendar = this.findViewById(R.id.buttonShowCalendar);
		buttonShowCalendar.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				final Dialog dialog = new Dialog(view.getContext(), android.R.style.Theme_Black_NoTitleBar_Fullscreen);
				FDMonthView v = new FDMonthView(view.getContext(), 1, 2022, new FDApptData[0], new FDApptData[0]);
				v.setMultiSelection(false);
				v.setCancelHandler(new OnCancelListener() {
					@Override
					public void onCancel(FDMonthView view) {
						Log.i("TAG", "onCancel");
						dialog.dismiss();
					}
			   	});
				v.setSaveHandler(new OnSaveListener() {
					@Override
					public void onSave(FDMonthView view) {
						FDApptData[] data = view.getSelected();
						for (int i = 0; i < data.length; i++) {
							Log.i("TAG", "onSave " + new Integer(data[i].day).toString());
						}
						dialog.dismiss();
					}
				});
				v.setSeeActivityTimesHandler(new OnSeeActivityTimesListener() {
					@Override
					public void onSeeActivityTimes(FDMonthView view) {
						Log.i("TAG", "onSeeActivityTimes");
						dialog.dismiss();
					}
				});
				dialog.setContentView(v);
				dialog.setTitle("Calendar");
				dialog.show();
			}
		});
	}
}
